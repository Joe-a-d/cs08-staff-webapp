from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.validators import EmailValidator, FileExtensionValidator, URLValidator, validate_image_file_extension
from .validators import (
    validate_moodle_link, validate_empty_field, validate_user_email,
)

'''
Models.py 

Creates structures for all of the different data needed.
- Subjects
- Tags
- Announcements
- Socials
- UserProfile
- LectureSection
- Subsection
- Content
- ContentLabel
- Textbook
'''

class Subject(models.Model):
    YEAR_CHOICES = (
        ("1", 1),
        ("2", 2),
        ("3", 3),
        ("4", 4),
        ("5", 5),
    )

    title = models.CharField(max_length=50, validators=[validate_empty_field])
    year = models.CharField(max_length=1, choices=YEAR_CHOICES)
    image = models.ImageField(upload_to='subject_images', validators=[validate_image_file_extension])  # default
    moodle_link = models.URLField(null=True, validators=[URLValidator, validate_moodle_link])

    def __str__(self):        
        return f'{self.title} - Year {self.year}'



class Tags(models.Model):
    name = models.CharField(max_length=15, validators=[validate_empty_field])


class Announcements(models.Model):
    title = models.CharField(max_length=30, validators=[validate_empty_field])
    content = models.TextField(validators=[validate_empty_field])

    class Meta:
        verbose_name_plural = 'Announcements'

    def __str__(self):
        return self.title


class Socials(models.Model):
    twitter = models.CharField(max_length=15, null=True, blank=True, unique=True)
    instagram = models.CharField(max_length=30, null=True, blank=True, unique=True)

    def clean(self):
        # at least one field should not be empty
        if self.twitter == None and self.instagram == None:
            raise ValidationError("Both fields are empty which is not allowed.")
        elif self.twitter != None and self.instagram != None:
            if self.twitter.isspace() and self.instagram.isspace():
                raise ValidationError("Both fields are empty which is not allowed.")


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    firebase_uid = models.CharField(max_length=5000, null=True, unique=True, validators=[validate_empty_field])
    email = models.EmailField(unique=True, validators=[EmailValidator, validate_user_email])
    can_edit = models.ManyToManyField(Subject, related_name='editors')
    is_taken = models.ManyToManyField(Subject, related_name='students')
    created_at = models.DateTimeField(auto_now_add=True)


class LectureSection(models.Model):
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    title = models.CharField(max_length=50, validators=[validate_empty_field])
    description = models.TextField(null=True)
    created_at = models.DateTimeField(null=True, blank=True)


class Subsection(models.Model):
    lecture_section = models.ForeignKey(LectureSection, on_delete=models.CASCADE, related_name='subsections')
    title = models.CharField(max_length=50, validators=[validate_empty_field])
    created_at = models.DateTimeField(null=True, blank=True)


class Content(models.Model):
    subsection = models.ForeignKey(
        Subsection, on_delete=models.CASCADE, related_name='content')
    file = models.FileField()
    file_type = models.CharField(max_length=5)
    due_date = models.DateField(null=True, blank=True)
    created_at = models.DateTimeField(null=True, blank=True)
    def clean(self):
        if self.file_type != self.file.name.split('.')[-1]:
            if '.' not in self.file.name:
                self.file_type = None
            else:
                self.file_type = self.file.name.split('.')[-1]

        if self.file.name.isspace():
            raise ValidationError("File field contains only whitespaces.")


class ContentLabel(models.Model):
    user = models.ForeignKey(
        UserProfile, on_delete=models.CASCADE, related_name='users')
    content = models.ForeignKey(
        Content, on_delete=models.CASCADE, related_name='labels')
    isDone = models.BooleanField(default=False,)
    isBookmark = models.BooleanField(default=False,)
    reminder = models.DateField(null=True, blank=True)
    tags = models.ManyToManyField(Tags,)


class Textbook(models.Model):
    subject = models.ManyToManyField(Subject)
    title = models.CharField(max_length=40, validators=[validate_empty_field])
    author = models.CharField(max_length=40, null=True)
    url = models.URLField(validators=[URLValidator])
    file = models.FileField(null=True)
    image = models.ImageField(
        upload_to='textbook_images',
        validators=[validate_image_file_extension]
    )  # default


class UsefulLink(models.Model):
    name = models.CharField(max_length=30)
    url = models.URLField()
