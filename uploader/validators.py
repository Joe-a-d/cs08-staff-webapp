from django.core.exceptions import ValidationError
from django.utils import timezone

'''
Validators.py

Validates moodle links, glasgow emails and makes sure fields are not empty
'''

def validate_moodle_link(value):
    if 'moodle.gla.ac.uk' not in value:
        raise ValidationError('%s is not a valid moodle link.' % value, params={'value': value})


def validate_empty_field(value):
    if value.isspace():
        raise ValidationError('Field contains only whitespaces.', params={'value': value})


def validate_user_email(value):
    if 'gla.ac.uk' not in value and 'glasgow.ac.uk' not in value:
        raise ValidationError('%s is not a valid email.' % value, params={'value': value})
