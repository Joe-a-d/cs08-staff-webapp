from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from api.serializers import (
    SocialsSerializer, AnnouncementsSerializer)
from uploader.models import Socials, Announcements
from populate import clean_up


class GetTimelineTest(APITestCase):
    """ Test the GET Socials and GET Announcements requests"""

    @classmethod
    def tearDownClass(cls):
        clean_up()

    def setUp(self):
        Socials.objects.create(twitter="tw1", instagram="ig1")
        Socials.objects.create(twitter="tw2", instagram="ig2")
        Announcements.objects.create(
            title="title 1", content="Kinfolk tote bag synthasymmetrical"
            "normcore disrupt. Kinfolk kogi irony PBR&B franzen consectetur"
            "readymade pork belly ennui hot chicken meditation brunch"
            "hexagon DIY succulents.")

        Announcements.objects.create(
            title="title 2", content="Kinfolk tote bag synthasymmetrical"
            "normcore disrupt. Kinfolk kogi irony PBR&B franzen consectetur"
            "readymade pork belly ennui hot chicken meditation brunch"
            "hexagon DIY succulents.")

    def test_get_all_socials(self):
        """
        The user calls the endpoint socials/ and all "Socials" objects are
        returned
        """

        response = self.client.get(reverse('get_socials'))
        socials = Socials.objects.all()
        serializer = SocialsSerializer(socials, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_all_socials(self):
        """
        The user tries to post to endpoint socials/ and gets a 405
        """

        response = self.client.post(reverse('get_socials'))
        self.assertEqual(response.status_code,
                         status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_get_all_announcements(self):
        """
        The user calls the endpoint socials/ and all "Socials" objects are
        returned
        """

        response = self.client.get(reverse('get_announcements'))
        announcements = Announcements.objects.all()
        serializer = AnnouncementsSerializer(socials, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_all_announcements(self):
        """
        The user tries to post to endpoint socials/ and gets a 405
        """

        response = self.client.post(reverse('get_announcements'))
        self.assertEqual(response.status_code,
                         status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_unauth_socials_announcements(self):
        """
        An unauthenticated user tries to get and post content
        """

        # TBT when auth is implemented
        pass
