from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from api.serializers import UsefulLinkSerializer
from uploader.models import UsefulLink
from populate import clean_up


class GetUsefulLinkTest(APITestCase):
    """ Test the GET UsefulLink request"""

    @classmethod
    def tearDownClass(cls):
        clean_up()

    def setUp(self):
        domains = ["google", "netflix", "twitter" "theguardian"]
        for domain in domains:
            UsefulLink.objects.create(
                name=domain,
                url=f"https://{domain}.com")

    def test_get_all_links(self):
        response = self.client.get(reverse('get_links'))
        links = UsefulLink.objects.all()
        serializer = UsefulLinkSerializer(links, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
