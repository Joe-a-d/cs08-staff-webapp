from os import path
import datetime
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.test import APIRequestFactory
from api.serializers import (
    SubjectSerializer, TextbookSerializer, LectureSectionSerializer,
    ContentSerializer, ContentLabelSerializer, TagSerializer
)
from uploader.models import (
    Subject, Textbook, LectureSection, Subsection, Content, ContentLabel,
    Tags, UserProfile, User,
)
from populate import clean_up
from rest_framework.test import APIRequestFactory


class GetContentTest(APITestCase):
    """ Test the GET content requests"""

    @classmethod
    def setUpClass(cls):
        cls.r_factory = APIRequestFactory()

    @classmethod
    def tearDownClass(cls):
        clean_up()

    def setUp(self):
        self.user = User.objects.create_user(
            username='admin',
            password='psswd')

        self.profile = UserProfile.objects.create(
            user=self.user, firebase_uid="asdah23k432k4g",
            email="adminemail@yahoo.co.uk",)

        self.subject_book = Subject.objects.create(
            id=43, title="Subject 43", year=2,
            image="https://upload.wikimedia.org/wikipedia/commons/c/ca/"
            "Sign-Info-icon.png")

        self.subject_book_2 = Subject.objects.create(
            title="Subject 44", year=3,
            image="https://upload.wikimedia.org/wikipedia/"
            "commons/c/ca/Sign-Info-icon.png")

        self.textbook = Textbook.objects.create(
            title="Book Show", author="Chomsky",
            url="https://www.gla.ac.uk/media/Media_124293_smxx.pdf",
            image="https://upload.wikimedia.org/wikipedia/commons/c/ca/"
            "Sign-Info-icon.png",
            file="mock.pdf"
        )

        self.textbook.subject.add(self.subject_book)

        self.textbook_2 = Textbook.objects.create(
            title="Book Hide", author="Chomsky",
            url="https://www.gla.ac.uk/media/Media_124293_smxx.pdf",
            image="https://upload.wikimedia.org/wikipedia/commons/c/ca/"
            "Sign-Info-icon.png"
        )

        self.section = LectureSection.objects.create(
            subject=self.subject_book, title="Section 1",
            description="Description 1")

        self.subsection = Subsection.objects.create(
            lecture_section=self.section, title="Subsection 1.1.")

        self.content_rm = Content.objects.create(
            subsection=self.subsection,
            file=path.join('README.md'),
            file_type="md")

        self.content_bm = Content.objects.create(
            subsection=self.subsection,
            file=path.join('Procfile'),
            file_type="")

        self.label_rm = ContentLabel.objects.create(
            user=self.profile,
            content=self.content_rm,
            reminder=datetime.date(2021, 2, 10),)

        self.label_bm = ContentLabel.objects.create(
            user=self.profile,content=self.content_bm, isBookmark=True)

        self.tag1 = Tags.objects.create(name="tag1")
        self.tag2 = Tags.objects.create(name="tag2")
        self.label_bm.tags.add(self.tag1)


    def test_get_content_bookmarks(self):
        """ The user calls the endpoint `content?bookmarks` and all
        "ContentLabel" objects with `isBookmark=True`  are returned
        """
        response = self.client.get(reverse('get_content')+"?bookmarks=true")
        # uploader_content JOIN uploader_contentlabel
        bookmarks = Content.objects.filter(labels__isBookmark=True)
        serializer = ContentSerializer(bookmarks, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_content_bookmarks(self):
        """
        The user calls the endpoint `content?events` and all "ContentLabel"
        objects with `reminders!=null`  are returned
        """
        response = self.client.get(reverse('get_content')+"?events=true")
        # uploader_content JOIN uploader_contentlabel
        events = Content.objects.filter(labels__reminder__isnull=False)
        request = self.r_factory.post(reverse('get_content')+"?events=true")
        serializer = ContentSerializer(events, many=True, 
            context={'request': request})
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_label(self):
        """
        Test the PATCH ContentLabel requests
        """
        data = {'isBookmark': False, 'reminder': datetime.date(2021, 3, 10)}
        uid = self.label_bm.id
        response = self.client.patch(reverse(
            'update_content', kwargs={'uid': uid}),
            data=data)
        # all requests will be filtered by user, hence each content object
        # should have a single label
        label = Content.objects.get(id=uid).labels.get()
        for (k, v) in data.items():
            setattr(label, k, v)
        label.save()
        serializer = ContentLabelSerializer(label)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_append_tag_not_delete(self):
        """
        Test the PATCH request M-M field update ; Tag should be added
        without overwriting
        """
        data = {"tags": self.tag1.id}
        uid = self.label_bm.id
        response = self.client.patch(reverse(
            'update_content', kwargs={'uid': uid}),
            data=data)
        label = Content.objects.get(id=uid).labels.get()
        tag = Tags.objects.get(id=data['tags'])
        label.tags.add(tag)
        serializer = ContentLabelSerializer(label)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_remove_tag(self):
        """
        Test the PATCH request to disassociate a tag from a label
        """
        data = {"tags": self.tag2.id}
        uid = self.label_rm.id
        response = self.client.patch(reverse(
            'update_content', kwargs={'uid': uid})+"?remove=true",
            data=data)
        label = Content.objects.get(id=uid).labels.get()
        tag = Tags.objects.get(id=data['tags'])
        label.tags.remove(tag)
        serializer = ContentLabelSerializer(label)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_tag(self):
        """
        Test the POST request to create a new tag
        We test:
        - whether a tag was added by counting the number of Tag objects 
        in the DB
        - whether the data passed was used in the serializer
        - whether there were no validation issues on save by checking the
        status code
        """
        num_tags_before  = len(Tags.objects.all())
        # print(num_tags_before)
        # print(Tags.objects.all())
        data = {"name": "tag3"}
        response = self.client.post(reverse('create_tag'), data=data)
        num_tags_after = len(Tags.objects.all())
        self.assertEqual(num_tags_before+1, num_tags_after)
        self.assertEqual(response.data["name"], data["name"])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_absolute_path_returned(self):
        sid = self.subject_book.id
        subj = self.client.get(reverse('get_subjects')+str(sid), 
            follow=True).json()[0]
        text = self.client.get(reverse('get_textbooks', kwargs={'uid':sid})
            ).json()[0]
        content = self.client.get(reverse('get_content')
            ).json()[0]
        lec = self.client.get(reverse('get_lectures', kwargs={'uid':sid})
            ).json()[0]
        test_url = 'http://testserver/media'
        self.assertIn(test_url, subj['image'])
        self.assertIn(test_url, text['image'])
        self.assertIn(test_url, text['file'])
        self.assertIn(test_url, content['file'])
        self.assertIn(test_url, lec['subsections'][0]['content'][0]['file'])


        
