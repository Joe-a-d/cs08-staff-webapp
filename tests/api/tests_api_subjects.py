from os import path
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.test import APIRequestFactory
from api.serializers import (
    SubjectSerializer, TextbookSerializer, LectureSectionSerializer,)
from uploader.models import (Subject, Textbook, LectureSection, Subsection,
                             Content,)
from populate import clean_up



class GetSubjectsTest(APITestCase):
    """ Test the GET Socials and GET Announcements requests """

    @classmethod
    def setUpClass(cls):
        cls.r_factory = APIRequestFactory()

    @classmethod
    def tearDownClass(cls):
        clean_up()

    def setUp(self):
        subject_book = Subject.objects.create(
            id=43, title="Subject 43", year=2,
            image="https://upload.wikimedia.org/wikipedia/commons/c/ca/"
            "Sign-Info-icon.png")
        Subject.objects.create(
            title="Subject 44", year=3,
            image="https://upload.wikimedia.org/wikipedia/"
            "commons/c/ca/Sign-Info-icon.png")
        textbook = Textbook.objects.create(
            title="Book Show", author="Chomsky",
            url="https://www.gla.ac.uk/media/Media_124293_smxx.pdf",
            image="https://upload.wikimedia.org/wikipedia/commons/c/ca/"
            "Sign-Info-icon.png"
        )
        # associate textbook with subject with id=43
        textbook.subject.add(subject_book)
        Textbook.objects.create(
            title="Book Hide", author="Chomsky",
            url="https://www.gla.ac.uk/media/Media_124293_smxx.pdf",
            image="https://upload.wikimedia.org/wikipedia/commons/c/ca/"
            "Sign-Info-icon.png"
        )
        # create section and associate it with subject with id=43
        section = LectureSection.objects.create(
            subject=subject_book, title="Section 1",
            description="Description 1")
        # create subsection and associate it with section with section 1
        subsection = Subsection.objects.create(
            lecture_section=section, title="Subsection 1.1.")
        # content
        # create content and associate it with subsection 1.1.
        content = Content.objects.create(
            subsection=subsection, file=path.join('README.md'), file_type="md")

    def test_get_year_subjects(self):
        """
        The user calls the endpoint `subjects?year=<year>` and all
        "Subject" objects with year=<year> are returned
        """
        request = self.r_factory.post(reverse('get_subject', kwargs={'uid': 43}))
        # here we pass a query string parameter
        response = self.client.get(reverse('get_subjects')+'?year=3')
        subjects = Subject.objects.filter(year=3)
        serializer = SubjectSerializer(subjects, many=True,
            context={'request': request})
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_subject(self):
        """
        The user calls the endpoint `subjects/<uid:int>` and a "Subject"
        object matching the uid is returned
        """

        # here we pass a url parameter, note the difference with the above
        request = self.r_factory.post(reverse('get_subject', kwargs={'uid': 43}))
        response = self.client.get(reverse('get_subject', kwargs={'uid': 43}))
        subject = Subject.objects.filter(id=43)
        serializer = SubjectSerializer(subject, many=True,
            context={'request': request})
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_subject_textbooks(self):
        """
        The user calls the endpoint `subjects/<uid:int>/textbooks/` and
        all "Textbook" objects associated with the subject with that uid are
        returned
        """
        request = self.r_factory.post(reverse('get_subject', kwargs={'uid': 43}))
        response = self.client.get(
            reverse('get_textbooks', kwargs={'uid': 43}))
        subject_book = Subject.objects.get(id=43)
        textbooks = Textbook.objects.filter(subject=subject_book)
        serializer = TextbookSerializer(textbooks, many=True,
            context={'request': request})
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_subject_lecture(self):
        """
        The user calls the endpoint `subjects/<uid:int>/lecture` and all
        the "Content" objects associated with the subject are retrieved,
        alongside their corresponding, sections and subsections
        """
        request = self.r_factory.post(reverse('get_subject', kwargs={'uid': 43}))
        response = self.client.get(reverse('get_lectures', kwargs={'uid': 43}))
        subject_lecture = Subject.objects.get(id=43)
        lecture = LectureSection.objects.filter(subject=subject_lecture)
        serializer = LectureSectionSerializer(lecture, many=True,
            context={'request': request})
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
