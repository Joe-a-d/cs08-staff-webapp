from django.test import TestCase
from django.urls import reverse
from django.core.files import File
from django.conf import settings
from uploader.models import UserProfile, Subject, User
import requests
import os
from firebase_admin import auth
from firebase_admin.exceptions import FirebaseError
from populate import clean_up


class ManagerTest(TestCase):

	@classmethod
	def setUpClass(cls):
		cls.firebase_app = settings.FIREBASE_APP
		cls.student_email = 'test@student.gla.ac.uk'
		cls.lecturer_email = 'lecturer_test@glasgow.ac.uk'
		cls.secretary_email = 'secretary_test@glasgow.ac.uk'
		cls.admin_email = 'admin_test@gla.ac.uk'
		cls.invalid_email = 'invalid_test@email.com'
		cls.django_inactive = 'inactive_test@gla.ac.uk'
		cls.pswd = 'pswd123'
		# delete user in Firebase
		users_to_delete = [user.uid for user in auth.list_users().users
						   if "test" in user.email]
		auth.delete_users(users_to_delete)
		# create users in Firebase
		auth.create_user(email=cls.student_email, password=cls.pswd)
		auth.create_user(email=cls.lecturer_email, password=cls.pswd)
		auth.create_user(email=cls.secretary_email, password=cls.pswd)
		auth.create_user(email=cls.admin_email, password=cls.pswd)
		auth.create_user(email=cls.django_inactive, password=cls.pswd)
		# create users in Django 
		cls.stu = User.objects.create(username=cls.student_email,
			email=cls.student_email,)
		cls.lec = User.objects.create(username=cls.lecturer_email,
			email=cls.lecturer_email,)
		cls.sec = User.objects.create(username=cls.secretary_email,
			email=cls.secretary_email, 
			is_staff=True)
		cls.admin = User.objects.create(username=cls.admin_email,
			email=cls.admin_email, 
			is_superuser=True)
		cls.inactive = User.objects.create(username=cls.django_inactive,
			email=cls.django_inactive,
			is_active=False)


	@classmethod
	def tearDownClass(cls):
		firebase_app = settings.FIREBASE_APP
		users_to_delete = [user.uid for user in auth.list_users().users
						   if "test" in user.email]
		auth.delete_users(users_to_delete)
		clean_up()

	def setUp(self):
		self.response = self.client.get(reverse('login'))

	def test_home_is_reachable(self):
		self.assertEqual(self.response.status_code, 200)

	def test_login_student_not_allowed(self):
		response = self.client.post(reverse('login'),
			data={'email': self.student_email,
			'password': self.pswd},follow=True)
		messages = [message.message for message in response.context['messages']]
		in_messages = any("Students" in string for string in messages)
		self.assertTrue(in_messages)

	def test_login_lecturer_allowed_redirected(self):
		response = self.client.post(reverse('login'),
			data={'email': self.lecturer_email,
			'password': self.pswd},follow=True)
		self.assertContains(response, 'uploader')

	def test_login_secretary_allowed_redirected(self):
		response = self.client.post(reverse('login'),
			data={'email': self.secretary_email,
			'password': self.pswd},follow=True)
		self.assertContains(response, 'Manage User Access')

	def test_login_admin_allowed_redirected(self):
		response = self.client.post(reverse('login'),
			data={'email': self.admin_email,
			'password': self.pswd},follow=True)
		self.assertContains(response, 'manage-modules')

	def test_invalid_login_username(self):
		response = self.client.post(reverse('login'),
			data={'email': self.invalid_email,
			'password': self.pswd},follow=True)
		messages = [message.message for message in response.context['messages']]
		in_messages = any("Invalid" in string for string in messages)
		self.assertTrue(in_messages)	

	def test_invalid_login_password(self):
		response = self.client.post(reverse('login'),
			data={'email': self.admin_email,
			'password': 'invalid'},follow=True)
		messages = [message.message for message in response.context['messages']]
		in_messages = any("Invalid" in string for string in messages)
		self.assertTrue(in_messages)

	def test_not_active_login(self):
		response = self.client.post(reverse('login'),
			data={'email': self.django_inactive,
			'password': self.pswd}, follow=True)
		messages = [message.message for message in response.context['messages']]
		in_messages = any("not allowed" in string for string in messages)
		self.assertTrue(in_messages)

	def test_login_middleware_redirects(self):
		# might write a helper to get all views from all apps except API
		# but this should suffice for now
		rev = reverse('manager:register')
		response = self.client.get(rev)
		self.assertRedirects(response, f'/login/?next={rev}')

	def test_login_middleware_excludes(self):
		# same here
		response_api = self.client.get(reverse('get_socials')).status_code
		response_login = self.client.get(reverse('login')).status_code
		self.assertEqual(response_api, 200)
		self.assertEqual(response_login, 200)

	def test_login_middleware_blocks_lecturer(self):
		self.client.force_login(self.lec)
		response = self.client.get(reverse('manager:register')).status_code
		self.assertEqual(response, 403)

	def test_login_middleware_allows_lecturer_upload(self):
		# TODO uncomment when url mapped
		# response = self.client.get(reverse('uploader')).status_code
		# self.assertEqual(response, 200)
		pass

	def test_login_middleware_blocks_secretary_socials(self):
		# secretary is not allowed to access socials page
		self.client.force_login(self.sec)
		response = self.client.get(reverse('socials_announcements:socials')).status_code
		self.assertEqual(response, 403)

	def test_login_middleware_allows_secretary_register(self):
		# secretary is allowed to register users on firebase
		self.client.force_login(self.sec)
		response = self.client.get(reverse('manager:register')).status_code
		self.assertEqual(response, 200)

	def test_login_middleware_allows_secretary_revoke(self):
		# secretary is allowed to see the manager home page where to revoke users from firebase
		self.client.force_login(self.sec)
		response = self.client.get(reverse('manager:home')).status_code
		self.assertEqual(response, 200)





