from django.test import TestCase
from django.urls import reverse
from django.contrib.messages import get_messages
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from uploader.models import Socials
import os
import secrets
from populate import clean_up


class SocialsTest(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.logged_in_user = User.objects.create(
            username="log_admin@gla.ac.uk", 
            is_superuser=True)

    @classmethod
    def tearDownClass(cls):
        clean_up()


    def setUp(self):
        self.client.force_login(self.logged_in_user)
        self.token_twitter = os.getenv('TOKEN_TWITTER')
        self.endpoint_twitter_data = "https://api.twitter.com/2/tweets/search/recent"
        self.endpoint_twitter_user = "https://api.twitter.com/2/users/by/username/"
        self.headers_twitter = {"Authorization": f"Bearer {self.token_twitter}"}
        self.response_socials = self.client.get(
            reverse('socials_announcements:socials'))

    def test_home_is_reachable(self):
        self.assertEqual(self.response_socials.status_code, 200)


    def test_template_is_used(self):
        self.assertTemplateUsed(self.response_socials, 'manage-socials-access.html')

    def test_twitter_invalid_username_length(self):
        username = secrets.token_hex(16)  
        response = self.client.post(reverse('socials_announcements:socials'),
                                    {'twitter': username}, )
        self.assertContains(response, "at most 15")

    def test_twitter_invalid_username_non_alpha_num(self):
        username = "@#@23$@"
        response = self.client.post(reverse('socials_announcements:socials'),
                                    {'twitter': username}, )
        self.assertContains(response, "Invalid")

    def test_twitter_non_existing_user(self):
        response = self.client.post(reverse('socials_announcements:socials'),
                                {'twitter': 'nx8192309xx'}, )
        self.assertContains(response, "exists and is public")

    def test_twitter_user_is_fetched_and_data_redirected(self):
        response = self.client.post(reverse('socials_announcements:socials'),
                                    {'twitter': 'jack'}, )
        success = True
        try:
            payload = response.context['tweet_data']
        except:
            success = False
        self.assertTrue(success)

    def test_twitter_profile_is_protected(self):
        # this might break if mary changes her profile; no need for mock API
        response = self.client.post(reverse('socials_announcements:socials'),
                                    {'twitter': 'mary'}, )
        social_object = Socials.objects.filter(twitter='mary')
        messages = [m.message for m in get_messages(response.wsgi_request)]
        self.assertEqual(len(social_object), 0)
        self.assertIn('exists and is public', messages[0])


    def test_twitter_social_object_created_and_unique(self):
        response = self.client.post(reverse('socials_announcements:socials'),
                                    {'twitter': 'uofg'},)
        response2 = self.client.post(reverse('socials_announcements:socials'),
                                    {'twitter': 'uofg'},)
        social_object = Socials.objects.filter(twitter='uofg')
        self.assertTrue(len(social_object), 1)

    def test_instagram_url_returned(self):
        response = self.client.post(reverse('socials_announcements:socials'),
                                    {'instagram': 'uofglasgow'},)
        success = True
        try:
            payload = response.context['instagram_data']
        except:
            success = False
        self.assertTrue(success)

    def test_instagram_invalid_username_length(self):
        username = secrets.token_hex(31)  
        response = self.client.post(reverse('socials_announcements:socials'),
                                    {'instagram': username}, )
        self.assertContains(response, "at most 30")

    def test_instagram_invalid_username_non_alpha_num(self):
        username = "@#@23$@"
        response = self.client.post(reverse('socials_announcements:socials'),
                                    {'instagram': username}, )
        self.assertContains(response, "Invalid")

    def test_instagram_social_object_created_and_unique(self):
        response = self.client.post(reverse('socials_announcements:socials'),
                                    {'instagram': 'uofglasgow'},)
        response2 = self.client.post(reverse('socials_announcements:socials'),
                                    {'instagram': 'uofglasgow'},)
        social_object = Socials.objects.filter(instagram='uofglasgow')
        self.assertTrue(len(social_object), 1)

    def test_regex_form_validation(self):
        twitter="twitter_u"
        instagram="insta_user.name"
        response = self.client.post(reverse('socials_announcements:socials'),
                                    {'twitter': twitter, 
                                    'instagram': instagram},)
        social_object = Socials.objects.filter(twitter=twitter,
            instagram=instagram)
        self.assertTrue(len(social_object), 1)

    def test_socials_deleted(self):
        obj_to_delete = Socials.objects.create(id=193)
        response = self.client.post(reverse('socials_announcements:revoke'),
            {'uid': '193'})  # noqa: E128
        self.assertRaises(ObjectDoesNotExist, Socials.objects.get, 
            id=obj_to_delete.id)  # noqa: E128
