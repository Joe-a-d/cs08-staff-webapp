from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import WebDriverException
from django.test import LiveServerTestCase

class ManagingAccessTest(LiveServerTestCase):
    options = Options()
    options.add_argument('--headless');
    options.binary_location = "/Applications/Brave Browser.app/Contents/MacOS/Brave Browser"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.browser = webdriver.Chrome(options=cls.options)
        cls.browser.implicitly_wait(5)

    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()
        super().tearDownClass()


    def test_revoke_access(self):
    	# Jack access the managing access home page
    	self.browser.get(f"{self.live_server_url}/manager/")

    	# He notices a table with header `ID`, `Email` and `Subject`
    	table_headers = self.browser.find_elements_by_tag_name('th')
    	table_headers = [header.text for header in table_headers]
    	self.assertEqual(['ID', 'Email', 'Subject'], table_headers)

    	# He then clicks on the <BUTTON_LABEL> button and is redirected
        # to the appropriate page


        # He adds a new lecturer and is redirected to the managing access
        # home page. The new lecturer is now present on the table


        # He clicks the revoke button next to it and the lecturer is removed