from django.test import TestCase
import os


class EnvTest(TestCase):

    def test_env(self):
        ENVS = ['FIREBASE_API_KEY', 'GOOGLE_APPLICATION_CREDENTIALS',
                'TOKEN_TWITTER']
        IN_ENV = [env for env in ENVS if os.getenv(env)]
        self.assertCountEqual(IN_ENV, ENVS)