from django.core.exceptions import ValidationError
from uploader.models import *
from django.test import TestCase
from django.db import IntegrityError


class ModelValidationTest(TestCase):

    def test_subject_image_valid(self):
        """ 
        test the Subject image is valid or not
        WONT WORK since year(choices) would always fail
        """
        subject = Subject.objects.create(title="Subject1", year=1, moodle_link = "https://moodle.gla.ac.uk/course/view.php?id=0")

        # invalid image extension
        subject.image = ".\\dental_staff\\static\\resources\\content.docx"
        with self.assertRaises(ValidationError):
            subject.full_clean()

        # valid image extension
        subject.image = ".\\dental_staff\\static\\resources\\content.jpg"
        # subject.full_clean()

    def test_subject_moodlelink_valid(self):
        """ 
        test the Subject moodle_link is valid or not
        WONT WORK since year(choices) would always fail
        """
        subject = Subject.objects.create(title="Subject1", year=1, image = ".\\dental_staff\\static\\resources\\content.jpg")

        # invalid link
        subject.moodle_link = "a.jpg"
        with self.assertRaises(ValidationError):
            subject.full_clean()

        # invalid moodle link
        subject.moodle_link = "https://www.google.com/"
        with self.assertRaises(ValidationError):
            subject.full_clean()

        # valid moodle link
        subject.moodle_link = "https://moodle.gla.ac.uk/course/view.php?id=0"
        # subject.full_clean()

    def test_tag_name_valid(self):
        """ 
        test the Tag name is valid or not
        """
        # invalid tag name
        tag = Tags.objects.create(name=" ")
        with self.assertRaises(ValidationError):
            tag.full_clean()

        #valid tag name
        tag.name = "pdf"
        tag.full_clean()

    def test_announcements_title_valid(self):
        """ 
        test the Announcement title is valid or not
        """
        announcement = Announcements.objects.create(content="hello")

        # invalid announcement title
        announcement.title = " "
        with self.assertRaises(ValidationError):
            announcement.full_clean()

        # valid announcement
        announcement.title = "Title"
        announcement.full_clean()

    def test_announcements_content_valid(self):
        """ 
        test the Announcement content is valid or not
        """
        announcement = Announcements.objects.create(title="hello")

        # invalid announcement content
        announcement.content = " "
        with self.assertRaises(ValidationError):
            announcement.full_clean()

        # valid announcement
        announcement.content = "Content"
        announcement.full_clean()

    def test_socials_content_valid(self):
        """ 
        test the Socials twitter&instagram content is valid or not
        """
        # invalid social content
        social = Socials.objects.create(twitter=" ", instagram=" ")
        with self.assertRaises(ValidationError):
            social.full_clean()
        
        social.twitter = "twitter"
        social.instagram = " "
        social.full_clean()

        social.twitter = "  "
        social.instagram = "instagram"
        social.full_clean()

    def test_socials_unique_valid(self):
        """ 
        test the Socials twitter&instagram is valid(unique) or not
        """
        social1 = Socials.objects.create(twitter="twitter", instagram="instagram")
        with self.assertRaises(IntegrityError):
            social2 = Socials.objects.create(twitter="twitter", instagram="instagram")

    def test_profile_email_valid(self):
        """ 
        test the UserProfile email is valid or not
        """
        user = User.objects.create(username="admintest", email="admintest@glasgow.ac.uk")
        profile = UserProfile.objects.create(user=user, email=user.email, firebase_uid="lDAzdqchBfV2D05DRP3ZUxj2FPg1")# valid profile email
        profile.full_clean()

        profile.email = "1@gmail.com"
        with self.assertRaises(ValidationError):
            profile.full_clean()

        profile.email = "glasgow.ac.uk"
        with self.assertRaises(ValidationError):
            profile.full_clean()

    def test_profile_firebase_uid_empty(self):
        """ 
        test the UserProfile firebase_uid is empty or not
        """
        user = User.objects.create(username="admintest", email="admintest@glasgow.ac.uk")
        profile = UserProfile.objects.create(user=user, email=user.email, firebase_uid="lDAzdqchBfV2D05DRP3ZUxj2FPg1")# non-empty firebase_uid
        profile.full_clean()

        # empty firebase_uid
        profile.firebase_uid = "  "
        with self.assertRaises(ValidationError):
            profile.full_clean()

    def test_lecture_section_title_valid(self):
        """ 
        test the LectureSection title is valid or not
        """
        subject = Subject.objects.create(title="Subject1", year=1, moodle_link = "https://moodle.gla.ac.uk/course/view.php?id=0")
        lecture_section = LectureSection.objects.create(subject=subject, description="description")

        # valid lecture section title
        lecture_section.title = "Section 1"
        lecture_section.full_clean()

        # invalid lecture section title
        lecture_section.title = " "
        with self.assertRaises(ValidationError):
            lecture_section.full_clean()
        
    def test_subsection_title_valid(self):
        """ 
        test the Subection title is valid or not
        """
        subject = Subject.objects.create(title="Subject1", year=1, moodle_link = "https://moodle.gla.ac.uk/course/view.php?id=0")
        lecture_section = LectureSection.objects.create(subject=subject, description="description", title="Section 1")
        subsection = Subsection.objects.create(lecture_section=lecture_section)

        # valid subsection title
        subsection.title = "Lecture Recordings"
        subsection.full_clean()

        # invalid subsection title
        subsection.title = " "
        with self.assertRaises(ValidationError):
            subsection.full_clean()

    def test_content_file_type_mismathced(self):
        """ 
        test the mismathced Content file type 
        """
        subject = Subject.objects.create(title="Subject1", year=1, moodle_link = "https://moodle.gla.ac.uk/course/view.php?id=0")
        lecture_section = LectureSection.objects.create(subject=subject, description="description", title="Section 1")
        subsection = Subsection.objects.create(lecture_section=lecture_section, title="Lecture Recordings")
        content = Content.objects.create(subsection=subsection, file=".\\dental_staff\\static\\resources\\content.jpg")

        # mismatched file_type
        content.file_type = "pdf"        
        content.full_clean()
        # should be matched after clean()
        self.assertEquals(content.file_type, "jpg")

    def test_content_file_empty(self):
        """ 
        test the Content file is empty or not 
        """
        subject = Subject.objects.create(title="Subject1", year=1, moodle_link = "https://moodle.gla.ac.uk/course/view.php?id=0")
        lecture_section = LectureSection.objects.create(subject=subject, description="description", title="Section 1")
        subsection = Subsection.objects.create(lecture_section=lecture_section, title="Lecture Recordings")
        content = Content.objects.create(subsection=subsection, file_type="jpg",)

        # non-empty file
        content.file = ".\\dental_staff\\static\\resources\\content.jpg"      
        content.full_clean()

        # empty file
        content.file = " "
        with self.assertRaises(ValidationError):
            content.full_clean()

    def test_textbook_title_valid(self):
        """ 
        test the Textbook title is valid or not 
        """
        subject = Subject.objects.create(title="Subject1", year=1, moodle_link = "https://moodle.gla.ac.uk/course/view.php?id=0")
        textbook = Textbook.objects.create(author="author", url="https://moodle.gla.ac.uk/course/view.php?id=0", file=".\\dental_staff\\static\\resources\\content.pdf", image=".\\dental_staff\\static\\resources\\content.jpg")
        textbook.subject.add(subject)

        # valid title
        textbook.title = "Textbook1"
        textbook.full_clean()

        # invalid title
        textbook.title = " "
        with self.assertRaises(ValidationError):
            textbook.full_clean()

    def test_textbook_url_valid(self):
        """ 
        test the Textbook url is valid or not 
        """
        subject = Subject.objects.create(title="Subject1", year=1, moodle_link = "https://moodle.gla.ac.uk/course/view.php?id=0")
        textbook = Textbook.objects.create(author="author",title = "Textbook1", file=".\\dental_staff\\static\\resources\\content.pdf", image=".\\dental_staff\\static\\resources\\content.jpg")
        textbook.subject.add(subject)

        # valid url
        textbook.url="https://moodle.gla.ac.uk/course/view.php?id=0"
        textbook.full_clean()

        # invalid url
        textbook.url = " "
        with self.assertRaises(ValidationError):
            textbook.full_clean()