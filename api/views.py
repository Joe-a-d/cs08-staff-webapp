from rest_framework import viewsets, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import *
from uploader.models import Socials, Subject, Announcements, Textbook, Content, LectureSection, Subsection, Tags

@api_view(['GET'])
def socials_get(request):
    """
    Retrieves all "Socials" objects
    
    Fields:

    twitter (string) : a twitter handle
    
    instagram (string) : an instagram handle

    """

    socials = Socials.objects.all()
    serializer = SocialsSerializer(socials, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def announcements_get(request):
    """

    Retrieves all "Announcements" objects
    
    Fields:

    title (string) 

    """

    announcements = Announcements.objects.all()
    serializer = AnnouncementsSerializer(announcements, many=True)
    return Response(serializer.data)
    
@api_view(['GET'])
def subjects_get(request, uid=None):
    """
    Retrieves all "Subject" objects, within a given year
    year (int) : the year associated with the subject

    image (image) : the image to be displayed

    id (int)

    content (string) 

    """

    year = request.query_params.get('year', None)
    if year:
        subjects = Subject.objects.filter(year=int(year.strip('/')))
    else:
        subjects = Subject.objects.filter(id=uid)
    serializer = SubjectSerializer(subjects, many=True,  context={'request':request})
    return Response(serializer.data)


@api_view(['GET'])
def textbooks_get(request, uid=None):
    """
    Retrieves all "Textbook" objects associated with the subject with `id=uid`

    Fields:

    title (string)

    author (string)

    url (string)

    file : the location of the file in the DB

    image : the image to be displayed 

    """

    subject = Subject.objects.get(id=uid)
    textbooks = Textbook.objects.filter(subject=subject)
    serializer = TextbookSerializer(textbooks, many=True, context={'request':request})
    return Response(serializer.data)

@api_view(['GET'])
def lectures_get(request, uid=None):
    """
    Retrieves all "Textbook" objects associated with the subject with `id=uid`

    Fields:

    title (string)

    author (string)

    url (string)

    file : the location of the file in the DB

    image : the image to be displayed 

    """

    subject = Subject.objects.get(id=uid)
    lectures = LectureSection.objects.filter(subject=subject)
    serializer = LectureSectionSerializer(lectures, many=True, context={'request':request})
    return Response(serializer.data)

@api_view(['GET'])
def content_get(request):
    """
    Retrieves all "Content" objects whose associated "ContentLabel" objects have their bookmark
    and/or reminder flags set. If no query parameters are passed it returns all "Content" objects,
    though there should be no use for the latter

    Fields:

    subsection (int) : subsection id

    file (string) 

    file_type (string)

    due_date (datetime)

    labels :

            content (int) : content id

            isDone (bool) : flag to (un)set content as done

            isBookmark (bool) : flag to (un)set content as bookmark

            reminder (datetime) : flag to (un)set content as reminder and its date

            tags (id) : Tag object

    """

    bookmarks = request.query_params.get('bookmarks', None)
    events = request.query_params.get('events', None)
    if events:
        content = Content.objects.filter(labels__reminder__isnull=False)
    elif bookmarks:
        content = Content.objects.filter(labels__isBookmark=True)
    else:
        content = Content.objects.all()
    serializer = ContentSerializer(content, many=True, context={'request':request})
    return Response(serializer.data)


@api_view(['PATCH'])
def content_update(request, uid=None):
    """
    Updates all "ContentLabel" objects via `PATCH` requests

    Editable fields:

    isDone (bool) : flag to (un)set content as done

    isBookmark (bool) : flag to (un)set content as bookmark

    reminder (datetime) : flag to (un)set content as reminder and its date

    tags (id) : Tag object
    """
    label = Content.objects.get(id=uid).labels.get()
    remove = request.query_params.get('remove', None)
    try:
        tag = Tags.objects.get(id=request.data.get('tags'))
    except:
        remove = False
        tag = False
    if remove:
        label.tags.remove(tag)
        serializer = ContentLabelSerializer(label, partial=True)
        return Response(serializer.data)
    if tag:
        label.tags.add(tag)
        serializer = ContentLabelSerializer(label, partial=True)
        return Response(serializer.data)
    serializer = ContentLabelSerializer(label, data=request.data, partial=True)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data)
    return Response(status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def content_tag(request):
    """
    Creates a new "Tag" object

    Fields:

    name (string)

    """

    serializer = TagSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def useful_link_get(request):
    links = UsefulLink.objects.all()
    serializer = UsefulLinkSerializer(links, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def delete_module(request):
	id = request.query_params.get('id', None)
	entry = Subject.objects.get(id=id)
	entry.delete()
	return Response(status.HTTP_200_OK);

@api_view(['GET'])
def delete_content(request):
	id = request.query_params.get('id', None)
	entry = Content.objects.get(id=id)
	entry.delete()
	return Response(status.HTTP_200_OK);

@api_view(['GET'])
def delete_subsection(request):
	id = request.query_params.get('id', None)
	entry = Subsection.objects.get(id=id)
	entry.delete()
	return Response(status.HTTP_200_OK);

@api_view(['GET'])
def delete_lecture_section(request):
	id = request.query_params.get('id', None)
	entry = LectureSection.objects.get(id=id)
	entry.delete()
	return Response(status.HTTP_200_OK);
