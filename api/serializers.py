from rest_framework import serializers
from uploader.models import (
    Socials, Announcements, Subject, Textbook,
    LectureSection, Subsection, Content, ContentLabel, Tags, UsefulLink)


class SocialsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Socials
        fields = ["id", "twitter", "instagram"]


class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = ["id", "title", "year", "image", "moodle_link"]


class TextbookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Textbook
        fields = ["id", "title", "author", "url", "file", "image"]


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tags
        fields = ["id", "name"]


class ContentLabelSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContentLabel
        fields = ["id", "content", "isDone", "isBookmark", "reminder", "tags"]


class ContentSerializer(serializers.ModelSerializer):
    labels = ContentLabelSerializer(many=True)

    class Meta:
        model = Content
        fields = ["id", "subsection", "file",
                  "file_type", "due_date", "labels"]


class SubsectionSerializer(serializers.ModelSerializer):
    content = ContentSerializer(many=True)

    class Meta:
        model = Subsection
        fields = ["id", "title", "content"]


class LectureSectionSerializer(serializers.ModelSerializer):
    subsections = SubsectionSerializer(many=True)

    class Meta:
        model = LectureSection
        fields = ["id", "subject", "title", "description", "subsections"]


class AnnouncementsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Announcements
        fields = ["id", "title", "content"]


class UsefulLinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = UsefulLink
        fields = ["id", "name", "url"]
