from django import forms
from django.forms import ValidationError
from uploader.models import Subject

'''
Forms.py 

Creates the form to allow users to upload a file with user details to 
allow them access to the app

'''

class UserCsvForm(forms.Form):
	email = forms.EmailField(required=False)
	file = forms.FileField(required=False)
	is_lecturer = forms.BooleanField(required=False)
	is_secretary = forms.BooleanField(required=False)
	is_admin = forms.BooleanField(required=False)
	# TODO
	# Add redirect link to subject creation page
	subjects = forms.ModelMultipleChoiceField(
		queryset=Subject.objects.all(), 
		required=False,
		help_text="If you can't see the subject you are looking for, you can "
		"create one ", )

	def clean(self):
		"""Custom form validation method
		
		The following checks are performed

		1. File extension is valid
		2. At least one field is not empty
		3. Email is valid
		4. Permissions are valid and the user is allowed to set them
		
		Returns:
			[dictionary] -- dict containing cleaned (normalized) data 
		
		Raises:
			ValidationError -- see checks above
		"""
		email = self.cleaned_data.get('email')
		file = self.cleaned_data.get('file')
		is_lecturer = self.cleaned_data.get('is_lecturer')
		is_secretary = self.cleaned_data.get('is_secretary')
		is_admin = self.cleaned_data.get('is_admin')
		subjects = self.cleaned_data.get('subjects')

		valid_extensions = ['xlsx', 'xlsm', 'csv', 'xls']

		if file:
			extension = file.name.split('.')[-1]
			if extension  not in valid_extensions:
				raise forms.ValidationError("Invalid file format."
					f"The only valid formats are {valid_extensions}")
		if not email and not file:
			raise forms.ValidationError("Please choose at least one")
		if email != "" and ("gla.ac.uk" not in email 
								and "glasgow.ac.uk" not in email):
			raise forms.ValidationError("A valid GU email address is required")
		
		
		if (is_lecturer and is_secretary or 
			(is_secretary and subjects.exists())):
			raise forms.ValidationError("Secretaries should not "
				"have access to course materials. "
				"You can make a lecturer an admin "
				"but note that this user will have "
				"full access rights")
		if is_secretary and is_admin:
			raise forms.ValidationError("An admin has all the permissions of "
				"a secretary. Please choose admin only")
		if is_lecturer and not subjects.exists():
			raise forms.ValidationError("A lecturer requires a subject")
		return self.cleaned_data