from django.apps import AppConfig

'''
Apps.py

Registering manager app
'''

class ManagerConfig(AppConfig):
    name = 'manager'
