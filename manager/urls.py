from django.urls import path
from . import views

app_name = 'manager'

'''
Registering the urls for managers
'''

urlpatterns = [
    path('', views.home, name="home"),
    path('register/', views.register, name="register"),
    path('revoke/', views.revoke, name="revoke"),
]
