import csv
import codecs
import secrets
import requests
import os
from django.shortcuts import render
from django.contrib import messages
from django.contrib.auth.models import User
from django.http import (
    HttpResponse, HttpResponseBadRequest, JsonResponse, HttpResponseForbidden
    )
from django.contrib.admin.views.decorators import staff_member_required
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from .forms import UserCsvForm
from uploader.models import UserProfile
from firebase_admin import auth
from firebase_admin.exceptions import FirebaseError
from firebase_admin._auth_utils import UserNotFoundError
import pandas as pd
import re


'''
Views.py 

All these views deal with the actions and logic of each page.
'''

def home(request):
    return render(request, 'manage_access.html', context={
        'users': UserProfile.objects.all()})

def register(request):
    """Adds users to Firebase and Django's DB
    
    Besides form handling and validation and template rendering this view is responsible for:

    1. Calling `file_handler` helper which validates and parses the file passed on the form
    2. Creates or activates Django users
    3. Creates UserProfile for created user
    4. Calls `give_permissions` (self explanatory)
    5. Calls `register_firebase` (see docstring for details)
    
    Error Handling:
    
    - A django message object is returned and no users are created if `file_handler` fails
    
    Arguments:
        request {[HttpRequest]} -- Django's HttpRequest object
    
    Returns:
        [HttpResponse] -- Django's HttpResponse object
    """
    if request.method == "POST":
        error_list = []
        user_form = UserCsvForm(request.POST, request.FILES)
        if user_form.is_valid():
            is_lecturer = user_form.cleaned_data['is_lecturer']
            is_secretary = user_form.cleaned_data['is_secretary']
            is_admin = user_form.cleaned_data['is_admin']
            try:
                file = request.FILES['file']
                emails = file_handler(file)
                if type(emails) == str:
                    messages.error(request, emails)
                    return render(request, 'manage-register.html', 
                        context={'user_form': user_form, })
                if user_form.cleaned_data['email'] != '':
                    email = [user_form.cleaned_data['email']]
                    emails += email
            except Exception as e:
                emails = [user_form.cleaned_data['email']]
            for email in emails:
                django_user = User.objects.get_or_create(
                    username=email, email=email)[0]
                # handles the case where a user might have been deleted
                # but is then reinstated
                if not django_user.is_active:
                    django_user.is_active = True
                    django_user.save()
                user_profile = UserProfile.objects.get_or_create(
                    user=django_user,
                    email=email)[0]
                give_permissions(
                    user=user_profile,
                    permissions=[is_lecturer, is_secretary, is_admin],
                    subjects=user_form.cleaned_data['subjects'])
                error_info = register_firebase(email, user_profile)
                error_list.append(str(error_info))
            if error_list:
                error = 'User with email {0} already exist on firebase. Grant access failed.'.format(', '.join(error_list))
                messages.error(request, error)
            else:
                messages.success(request, "The users were granted access"
                                "to the app(s)")
        else:
            return render(request, 'manage-register.html', context={'user_form': user_form})
    else:
        user_form = UserCsvForm()
    return render(request, 'manage-register.html', context={'user_form': user_form})


def revoke(request):
    """Handles the ajax post request to revoke user access

    1. Set django user is_active flag to False
    2. Delete user from Firebase

    Currently it is able to handle Firebase DB and local DB being out of sync

    Arguments:
        request {HTTP} -- an HTTP request with `profile_id` as a query param

    Returns:
        {JsonResponse} 

        -- 200 if either the user was deleted or there was 
        nothing to delete

        -- 400 otherwise. The response includes JSON fields indicating 
        where the response was  unsuccessful so that a descriptive message 
        can be shown to the user.
            {
                "removed_django": false,
                "removed_firebase": false
            }

        This should be handled in the template

    """
    profile_id = request.POST.get('profile_id')
    uid = UserProfile.objects.get(id=profile_id).firebase_uid
    result = {}

    try:
        if uid is None:
            raise Exception

        user = UserProfile.objects.get(firebase_uid=uid).user
        revoker = request.user
        # revoker: secretary/lecturer are not allowed to delete the admin
        if user.is_superuser and (not revoker.is_superuser):
            result['error'] = 'Revokes not allowed. You don\'t have enough right.'
            return JsonResponse(result, status=403)
        user.is_active = False
        user.save()
    except ObjectDoesNotExist as e:
        pass
    except Exception as e:
        # Empty Firebase_id, which is NULL, directly removed from Local DB
        if uid is None:
            # first try to remove user from firebase(if exists)
            user = UserProfile.objects.get(id=profile_id).user
            email = user.email
            response = revoke_firebase_by_email(email)

            # removed from local
            user.delete()
            result['error'] = 'This user has an invalid firebase uid. Invalid user profile removed.'
        else:
            result['error'] = 'Some problems exists, cannot fetch the user records'

    response = revoke_firebase(uid)
    if response == ValueError:
        result['error'] = 'User ID is None, empty or malformed.'
    elif response == FirebaseError:
        result['error'] = 'An error occurs while deleting the user account from firebase.'
    elif response == UserNotFoundError:
        # invalid firebase_uid provided before,
        # directly remove the invalid user & profile from local DB
        # and remove user with the same email from firebase
        result['error'] = 'Invalid user profile removed. User with the same email removed from firebase.'
    else:
        pass

    if result:
        return JsonResponse(result, status=400)
    return JsonResponse(result, status=200)


# HELPERS

def file_handler(file):
    """Reads a byte encoded file object and converts each 
    line to a list element


    Arguments:
        file {InMemoryUploadedFile} -- a byte encoded file object

    Returns:
        [[string]] -- a list of email addresses as strings
    """

    try:
        df = pd.read_csv(file)
    except:
        df = pd.read_excel(file)
    df = df.filter(regex=re.compile('email', re.IGNORECASE))
    try:
        column = df.columns[0]
    except:
        return "The file does not contain a header with the word 'email' on it"
    df = df[df[column].str.contains('[gla|sgow].ac.uk$', regex=True)][column]
    return(df.tolist())


def register_firebase(email, user_profile):
    """Registers user in Firebase's DB and adds token to user profile

        1. make the appropriate API call
            a. we use firebase_admin.exceptions.FirebaseError to check 
            that the user doesn't already exists
            b. we log any other errors to server logs
        3. call :py:meth:`~reset_firebase` to send reset password email

    Arguments:
        emails [[string]] -- a list of email addresses as strings
    """
    existing_emails = []

    # requires pswd , otherwise reset password email is not sent
    pswd = secrets.token_urlsafe(8)
    try:
        user = auth.create_user(email=email, password=pswd)
    except FirebaseError as e:
        if e.code == "ALREADY_EXISTS":
            error_info = email
            return error_info
    user_profile.firebase_uid = user.uid
    user_profile.save()
    reset_firebase(email)

def reset_firebase(email):
    """Calls the API endpoint responsible for sending pswd reset emails

    Arguments:
        email {String} -- email address
        api_key {String} -- Firebases's API key
        api_endpoint {String} -- Firebase's API base endpoint
    """
    api_key = os.environ['FIREBASE_API_KEY']
    api_endpoint = "https://identitytoolkit.googleapis.com/v1/accounts"
    api_reset = api_endpoint + f":sendOobCode?key={api_key}"
    reset = requests.post(api_reset,
                          data={"requestType": "PASSWORD_RESET",
                                "email": email})

def revoke_firebase_by_email(email):
    """Calls the API endpoint responsible for deleting a user by email

    Arguments:
        email {str} -- a user email

    Returns:
        [HTTP] -- an HTTP response, if the deletion is successful
        [FirebaseError] -- Firebase exception, if the deletion is unsuccessful
    """
    try:
        user = auth.get_user_by_email(email)
        response = auth.delete_user(user.uid)
    except UserNotFoundError:
        # No user record found for the given email
        # We ONLY need to remove invalid user from local DB then
        response = UserNotFoundError
    except ValueError as e:
        # User ID is None, empty or malformed.
        response = e
    except FirebaseError as e:
        # an error occurs while deleting the user account.
        response = e
    return response


def revoke_firebase(uid):
    """Calls the API endpoint responsible for deleting a user

    Arguments:
        uid {str} -- a firebase uid

    Returns:
        [HTTP] -- an HTTP response, if the deletion is successful
        [FirebaseError] -- Firebase exception, if the deletion is unsuccessful
    """
    try:
        # note: firebase_uid passed here might be a invalid one
        response = auth.delete_user(uid)
        user_id = UserProfile.objects.get(firebase_uid=uid).user_id
        User.objects.get(id=user_id).delete()
    except UserNotFoundError:
        # No user record found for the given identifier(uid), i.e. invalid firebase_uid provided in local DB
        # try to delete the user on firebase by email first
        email = UserProfile.objects.get(firebase_uid=uid).email
        user_on_firebase = auth.get_user_by_email(email)
        auth.delete_user(user_on_firebase.uid)
        # Then useless user profiles & user, directly removed from local DB
        user_id = UserProfile.objects.get(firebase_uid=uid).user_id
        User.objects.get(id=user_id).delete()

        response = UserNotFoundError
    except ValueError as e:
        # User ID is None, empty or malformed.
        response = e
    except FirebaseError as e:
        # an error occurs while deleting the user account.
        response = e
    return response


def give_permissions(user, permissions, subjects):
    """Assigns editing permissions to users

    Arguments:
        user {Model} -- a UserProfile Model object
        permissions {[Boolean]} -- a list of booleans, representing the 
        permission levels
            permissions[0] -- is_lecturer
            permissions[1] -- is_secretary
            permissions[2] -- is_admin
        subjects {[Model]} -- a list of Subject Model objects
    """

    user.can_edit.add(*subjects)
    if permissions[1]:
        user.user.is_staff = True
    if permissions[2]:
        user.user.is_superuser = True
    user.save()
    user.user.save()
