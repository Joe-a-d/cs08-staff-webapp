
# CS08 Staff Web App

## Setup

### Create and activate virtual environment

E.g. [by using venv](https://docs.python.org/3/library/venv.html) or any other virtual environment and then activating it.

### Install dependencies

Run

```shell
pip install -r requirements.txt
```

### Add local dev settings

i.e. `dev.py` into `settings/`.

## Running the web app

Inside of `dental_staff/` run:

```shell
python manage.py makemigrations
python manage.py migrate
python manage.py runserver
```

## Running the web app tests

Inside of `dental_staff/` run:

```shell
python manage.py test
```

## Deployment

The app is currently deployed at https://uofgdental.herokuapp.com/

For instructions on testing heroku deployments see the main wiki [here](https://stgit.dcs.gla.ac.uk/tp3-2020-CS08/cs08-main/-/wikis/Resources/Heroku)and for instructions on how to test the gitlab CI or to deploy locally using `gitlab-runner` see the wiki [here](https://stgit.dcs.gla.ac.uk/tp3-2020-CS08/cs08-main/-/wikis/Resources/Runners)


# API

The documentation for the API can be found [here](https://stgit.dcs.gla.ac.uk/tp3-2020-CS08/cs08-staff-webapp/-/wikis/REST-API-Documentation)


