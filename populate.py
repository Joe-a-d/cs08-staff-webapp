import django
import os
import sys
import random

# set env the same way as in manage.py
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dental_staff.settings.dev")
# make django
django.setup()

from faker import Faker
from factories import *
from uploader.models import *
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.files import File
from django.db.models import Q
from firebase_admin import auth

"""
Usage:
python populate.py clean
- to delete all the data

python populate.py
- to populate the data
"""

def create_useful_link():
    UsefulLink.objects.create(name="Dentistry Dictionary", 
        url="https://global.oup.com/academic/product/a-dictionary-of-dentistry-9780199533015")
    UsefulLink.objects.create(name="Google",
        url="https://google.com")
    print("UsefulLink objects generated")

f = Faker()

# def remove_test_users_from_firebase():
#     profile_queryset = UserProfile.objects.all()
#     for each in profile_queryset:
#         # add defensive path so that we can always run clean
#         try:
#             auth.delete_user(each.firebase_uid)
#         except Exception as e:
#             if e.code == "USER_NOT_FOUND":
#                 pass

def get_or_create_firebase_user(user_object):
    try:
        firebase_user = auth.create_user(
            email=user_object.email,
            email_verified=False,
            password=user_object.password,
            display_name=user_object.username,
            disabled=False
        )
    except Exception:
        firebase_user = auth.get_user_by_email(user_object.email)
    return firebase_user

def get_one_random_subject(subjects):
    """
    get one random subject from the given subjects queryset
    """
    return random.choice(subjects)

def get_one_random_tag(tags):
    """
    get one random tag from the given tags queryset
    """
    return random.choice(tags)

def get_some_random_subjects():
    """ 
    get some random subjects from the subjects queryset
    """
    subject_list = []
    subject_queryset = Subject.objects.all()

    num = random.randint(1, len(subject_queryset))
    for i in range(num):
        subject_list.append(random.choice(subject_queryset))

    return subject_list

def get_some_random_students(subject):
    """
    get some students' profile from the userprofile queryset
    """
    student_list = []
    users = UserProfile.objects.all()

    student_queryset = users.filter(
        Q(email__contains="student") &
        Q(is_taken=subject.id)
    )
    if len(student_queryset) < 0:
        return

    num = random.randint(1, len(student_queryset))
    for i in range(num):
        student_list.append(random.choice(student_queryset))

    return student_list

def get_some_random_objects(queryset):
    """ 
    used for get some random objects from the given queryset
    """
    num = random.randint(1, len(queryset))

    return queryset[:num]

def get_all_subject_content(subject):
    """ 
    Get all the contents of a specific subject
    """
    lectureSection_queryset = subject.lecturesection_set.all()
    content_list = []
    for each_section in lectureSection_queryset:
        subsection_queryset = each_section.subsections.all()
        for each_subsection in subsection_queryset:
            for each_content in each_subsection.content.all():
                content_list.append(each_content)
    return content_list

def clean_up():
    """ 
    Usage: python populate clean
    to delete all the data
    """
    Subject.objects.all().delete()
    Tags.objects.all().delete()
    Announcements.objects.all().delete()
    Socials.objects.all().delete()
    # remove_test_users_from_firebase()
    User.objects.all().delete()
    UserProfile.objects.all().delete()
    LectureSection.objects.all().delete()
    Subsection.objects.all().delete()
    Content.objects.all().delete()
    ContentLabel.objects.all().delete()
    Textbook.objects.all().delete()
    UsefulLink.objects.all().delete()

def make_objects():
    # make announcement
    AnnouncementsFactory.create_batch(size=10)
    print("%d Announcements generated" % Announcements.objects.all().count())

    # make socials with actual ins & twitter username
    Socials.objects.create(twitter="UofGdentsoc", instagram="glasgow_dental_student_society")
    Socials.objects.create(twitter="UofGDental", instagram="dental.student.network")

    # make subjects
    year_list = [1,2,3,4,5]
    for i in range(5):
        # make 2 subjects for each year, (default image - dental_staff/static/resources/book.jpg)
        SubjectFactory.create_batch(size=2, year=year_list[i])

        # Known Subject objects
        SubjectFactory(
            title='Mock Subject',
            image= SimpleUploadedFile(
                name='book.jpg', content=open("dental_staff/static/resources/Media_307388_smxx.jpg", 'rb').read()
            ),
        )

    print("%d Subjects generated" % Subject.objects.all().count())

    # make LectureSections
    for each in Subject.objects.all():
        # Known Lecture Section
        if each.title=='Mock Subject':
            LectureSectionFactory(
                description='Lecture Material',
                subject=each
            )
        LectureSectionFactory.create_batch(size=random.randint(1,5), subject=each)
    print("%d LectureSection generated" % LectureSection.objects.all().count())

    prefix='dental_staff/static/resources/'
    file_list = [
        prefix+'content.pdf', prefix+'content.pptx', 
        prefix+'content.docx', prefix+'content.xlsx', prefix+'content.jpg', prefix+'content.mp4'
    ]

    # make subsections
    for each in LectureSection.objects.all():
        SubsectionFactory.create_batch(size=random.randint(1,6), lecture_section=each)

        subsection_queryset =Subsection.objects.filter(lecture_section=each)

        # make contents
        for sub in subsection_queryset:
            # make 1 content.txt file
            ContentFactory.create(subsection=sub)
            # make contents of file_list variable, pdf, pptx, docx, xlsx, jpg, mp4
            for each_file in file_list:
                each_filename = each_file.split('/')[-1]
                each_filetype = each_file.split('/')[-1].split('.')[-1]
                ContentFactory.create(
                    subsection=sub,
                    file=SimpleUploadedFile(
                        name=each_filename, content=open(each_file, 'rb').read()), 
                    file_type=each_filetype
                )
    print("%d Subsection generated" % Subsection.objects.all().count())
    print("%d Content generated" % Content.objects.all().count())

    num = random.randint(2,5)
    for i in range(num):
        # make users - admin
        admin = UserFactory(
            username=factory.Sequence(lambda n: 'admin{0}'.format(n)),
            password='123456',
            email=factory.Sequence(lambda n: 'admin{0}@glasgow.ac.uk'.format(n)),
            is_staff=True,
            is_superuser=True,
        )
        firebase_admin = get_or_create_firebase_user(admin)
        UserProfileFactory(
            user=admin,
            firebase_uid=firebase_admin.uid
        )

        #make users - secretary
        secretary = UserFactory(
            username=factory.Sequence(lambda n: 'secretary{0}'.format(n)),
            password='123456',
            email=factory.Sequence(lambda n: 'secretary{0}@glasgow.ac.uk'.format(n)),
            is_staff=True,
            is_superuser=False,
        )
        firebase_secretary = get_or_create_firebase_user(secretary)
        UserProfileFactory(
            user=secretary,
            firebase_uid=firebase_secretary.uid
        )
    print("%d Admin generated" % User.objects.filter(username__contains='admin').count())
    print("%d Secretary generated" % User.objects.filter(username__contains='secretary').count())

    for i in range(num):
        # make users - lecturer
        lecturer = UserFactory(
            username=factory.Sequence(lambda n: 'lecturer{0}'.format(n)),
            password='123456',
            email=factory.Sequence(lambda n: 'lecturer{0}@glasgow.ac.uk'.format(n)),
            is_staff=False,
            is_superuser=False,
        )
        firebase_lecturer = get_or_create_firebase_user(lecturer)
        UserProfileFactory.create(
            user=lecturer,
            can_edit=tuple(get_some_random_subjects()),
            firebase_uid=firebase_lecturer.uid
        )

        # make users - students
        student = UserFactory(
            username=factory.Sequence(lambda n: 'student{0}'.format(n)),
            password='123456',
            is_staff=False, is_superuser=False,
            email=factory.Sequence(lambda n: 'student{0}@student.gla.ac.uk'.format(n)),
        )
        firebase_student = get_or_create_firebase_user(student)
        UserProfileFactory(
            user=student,
            is_taken=tuple(get_some_random_subjects()),
            firebase_uid=firebase_student.uid
        )
    print("%d Lecturer generated" % User.objects.filter(username__contains='lecturer').count())

    print("%d Student generated" % User.objects.filter(email__contains='student').count())

    # make Tags
    for i in range(4):
        TagsFactory()
    print("%d Tags generated" % Tags.objects.all().count())


    # Students can make ContentLabel
    # for random contents of the specific Subject
    # adding some contentLabel created by some random students
    for each in Subject.objects.all():
        if each.students.count()> 0:
            student_list = get_some_random_students(each)
            content_queryset = get_all_subject_content(each)
            content_list = get_some_random_objects(content_queryset)

            for student_profile in student_list:
                for each_content in content_list:
                    ContentLabelFactory(
                        content=each_content,
                        user=student_profile,
                        tags=tuple(get_some_random_objects(Tags.objects.all()))
                    )
    print("%d ContentLabel generated" % ContentLabel.objects.all().count())

    # make 20 textbooks for random subjects
    for i in range(20):
        TextbookFactory.create(
            subject=tuple(get_some_random_subjects())
        )
    print("%d Textbook generated" % Textbook.objects.all().count())

if __name__ == '__main__':
    if len(sys.argv)<2:
        make_objects()
        create_useful_link()
    elif sys.argv[1]=='clean':
        clean_up()