import random as rand
import factory
from factory import fuzzy
from uploader.models import Subject, Tags, Announcements, Socials, UserProfile, LectureSection, Subsection, Content, ContentLabel, Textbook
from faker import Faker
from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.files import File
from faker import Faker
import random

fake = Faker()
Faker.seed(0)

'''
Factories.py 

Generates dummy data for each field to test api

'''

class SubjectFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Subject

    title = factory.Sequence(lambda n: u'Subject %d' % n)
    year = factory.Iterator([1,2,3,4,5])
    image = SimpleUploadedFile(
        name='book.jpg', content=open("dental_staff/static/resources/book.jpg", 'rb').read()
    )
    moodle_link = factory.Sequence(lambda n: 'https://moodle.gla.ac.uk/course/view.php?id=%d' % n)


class TagsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Tags

    name = factory.Iterator(['pdf', 'txt', 'video', 'textbook'])


class AnnouncementsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Announcements

    title = factory.Sequence(lambda n: u'Title %d' % n)
    content = factory.Faker("text")


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Sequence(lambda n: 'username{0}'.format(n))
    password = factory.Faker('password')
    email = factory.Faker('email')
    is_staff = factory.Faker('boolean')


class UserProfileFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = UserProfile

    user = factory.SubFactory(UserFactory)
    firebase_uid = factory.Faker("pystr")
    email = factory.SelfAttribute('.user.email')

    @factory.post_generation
    def can_edit(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for subject in extracted:
                self.can_edit.add(subject)

    @factory.post_generation
    def is_taken(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for subject in extracted:
                self.is_taken.add(subject)


class LectureSectionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = LectureSection

    subject = factory.SubFactory(SubjectFactory)
    title = factory.Sequence(lambda n: u'Section %d' % n)
    description = factory.Faker("paragraph")
    created_at = factory.Faker('date_time_this_year', before_now=True)


class SubsectionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Subsection

    lecture_section = factory.SubFactory(LectureSectionFactory)
    title = factory.Iterator(['Lecture Slides', 'Lecture Recording', 'Required Reading', 'Recommended Reading', 'Textbook'])
    created_at = factory.Faker('date_time_this_year', before_now=True)


class ContentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Content

    subsection = factory.SubFactory(SubsectionFactory)
    file = SimpleUploadedFile(
        name='content.txt', content=open("dental_staff/static/resources/content.txt", 'rb').read(), content_type='text/plain'
    )
    file_type = 'txt'
    due_date = factory.LazyFunction(
        lambda: fake.date_this_year(after_today=True) if random.random() > 0.3 else None)
    created_at = factory.Faker('date_time_this_year', before_now=True)


class ContentLabelFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ContentLabel

    content = factory.SubFactory(ContentFactory)
    user = factory.SubFactory(UserProfileFactory)
    isDone = factory.Faker("boolean")
    isBookmark = factory.Faker("boolean")
    # if the label marked isDone then set reminder to NULL, otherwise would be a random time after now
    reminder = factory.Maybe(
        'isDone',
        yes_declaration=None,
        no_declaration=factory.Faker('date_time_this_year', after_now=True))
    tags = factory.SubFactory(TagsFactory)

    @factory.post_generation
    def tags(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for tag in extracted:
                self.tags.add(tag)


class TextbookFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Textbook

    title = factory.Sequence(lambda n: u'Textbook %d' % n)
    author = factory.Faker("name")
    url = factory.Sequence(lambda n: 'https://moodle.gla.ac.uk/course/view.php?id=%d' % n)
    file = SimpleUploadedFile(
        name='content.txt', content=open("dental_staff/static/resources/content.txt", 'rb').read(), content_type='text/plain'
    )
    image = SimpleUploadedFile(
        name='book.jpg', content=open("dental_staff/static/resources/book.jpg", 'rb').read()
    )

    @factory.post_generation
    def subject(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for subjects in extracted:
                self.subject.add(subjects)
