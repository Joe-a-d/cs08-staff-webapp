import os
import requests
import sys
from .forms import AnnouncementForm, ModuleForm, LectureSectionForm, SubsectionForm, ContentForm
from uploader.models import *
from datetime import datetime
from django.shortcuts import render, reverse, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages


'''
Views.py 

All these views deal with the actions and logic of each page.
'''

# Viewing posts (not finished yet)
def view_posts(request):
    return render(request, 'view-posts.html')
	
# managing socials access (not finished yet)
def manage_socials_access(request):
    return render(request, 'manage-socials-access.html')

# Allows users to login to the site, will only allow proper users to access the site
def user_login(request):
	ERROR_MESSAGE = f"You are not allowed to login. If you think this\
				is a mistake, please contact"

	if request.user.is_authenticated :
		messages.error(request, "You are already logged in")
		if request.user.is_superuser:
			return redirect(reverse('manage-modules'))
		if request.user.is_staff:
			return redirect(reverse('manager:home'))
		return redirect(reverse('uploader'))

	if request.method == 'POST':
		email = request.POST.get('email')
		password = request.POST.get('password')
		if "student" in email:
			messages.error(request,"Students are not allowed access")
			return redirect(reverse('login'))
		auth = firebase_authenticate(email=email, password=password)
		if auth.status_code != 200:
			messages.error(request,'Invalid login details.')
			return redirect(reverse('login'))
		try:
			user = User.objects.get(email=email)
		except Exception as e:
			return redirect(reverse('login'))
		if user.is_active:
			login(request, user)
			if user.is_superuser:
				return redirect(reverse('manage-modules'))
			if user.is_staff:
				return redirect(reverse('manager:home'))
			return redirect(reverse('uploader'))
		else:
			messages.error(request,ERROR_MESSAGE)
			return redirect(reverse('login'))
	else:
		return render(request, 'login.html',)

# Logs the user out of the site
def user_logout(request):
	logout(request)
	return redirect(reverse('login'))

# Manage socials main page
def manage_socials_landing(request):
    return render(request, 'manage_socials_landing.html')

# Allows users to make an announcement
def announcement(request):
    if request.method == 'POST':
        form = AnnouncementForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Announcement Submission Successful")
            form = AnnouncementForm()
    else:
        form = AnnouncementForm()
    return render(request, 'make-an-announcement.html', {'form': form})

# Authenticates users when they login
def firebase_authenticate(email, password):
	endpoint = "https://identitytoolkit.googleapis.com/v1/accounts"
	API_KEY = os.environ['FIREBASE_API_KEY']
	r = requests.post(f"{endpoint}:signInWithPassword?key={API_KEY}",
		params={'email':email, 'password':password})
	return r

# manage modules main page; shows all modules
def manage_modules(request):
    module_list = Subject.objects.all()
    context_dict = {}
    context_dict['modules'] = module_list
    return render(request, 'manage-modules.html', context=context_dict)

# Shows a specific module with all the sections, subsections, content, etc... assigned to that module
def view_module(request):
    module_list = Subject.objects.all()
    section_list = LectureSection.objects.all()
    subsection_list = Subsection.objects.all()
    content_list = Content.objects.all()
    context_dict = {}
    context_dict['modules'] = module_list
    context_dict['sections'] = section_list
    context_dict['content'] = content_list
    context_dict['subsections'] = subsection_list
    return render(request, 'view-module.html', context=context_dict)

# Allows users to add a new module the site
def add_module(request):
    if request.method == 'POST':
        form = ModuleForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, "Module Submission Successful")
            form = ModuleForm()
    else:
        form = ModuleForm()
    return render(request, 'add-module.html', {'form': form})

# Allows users to add a new section to a module
def add_lecture_section(request):
    if request.method == 'POST':
        moduleID = request.GET.get('id')
        module = Subject.objects.get(id=moduleID)
        form = LectureSectionForm(request.POST)
        if form.is_valid():
            new_lecture_section = form.save(commit=False)
            new_lecture_section.subject = module
            new_lecture_section.created_at = datetime.now()
            new_lecture_section.save()
            messages.success(request, "Lecture Section Submission Successful")
            form = LectureSectionForm()
    else:
        form = LectureSectionForm()
    return render(request, 'add-lecturesection.html', {'form': form})

# Allows users to add a new subsection to a module
def add_subsection(request):
    if request.method == 'POST':
        sectionID = request.GET.get('id')
        section = LectureSection.objects.get(id=sectionID)
        form = SubsectionForm(request.POST)
        if form.is_valid():
            new_section = form.save(commit=False)
            new_section.lecture_section = section
            new_section.created_at = datetime.now()
            new_section.save()
            messages.success(request, "Subsection Submission Successful")
            form = SubsectionForm()
    else:
        form = SubsectionForm()
    return render(request, 'add-subsection.html', {'form': form})

# Allows users to add new content to a section/subsection
def add_content(request):
    if request.method == 'POST':
        subsectionID = request.GET.get('id')
        subsection = Subsection.objects.get(id=subsectionID)
        form = ContentForm(request.POST, request.FILES)
        if form.is_valid():
            new_subsection = form.save(commit=False)
            new_subsection.subsection = subsection
            new_subsection.created_at = datetime.now()
            new_subsection.save()
            messages.success(request, "Content Submission Successful")
            form = ContentForm()
    else:
        form = ContentForm()
    return render(request, 'add-content.html', {'form': form})

# Allows users to change the uploaded content 
def edit_content(request):
    if request.method == 'POST':
        contentID = request.GET.get('id')
        content = Content.objects.get(id=contentID)
        form = ContentForm(request.POST, request.FILES, instance=content)
        if form.is_valid():
            new_content = form.save(commit=False)
            new_content.created_at = content.created_at
            new_content.subsection = content.subsection
            new_content.save()
            messages.success(request, "Content Update Successful")
            form = ContentForm()
    else:
        contentID = request.GET.get('id')
        content = Content.objects.get(id=contentID)
        form = ContentForm(instance=content)
    return render(request, 'edit-content.html', {'form': form})

# Allows users to edit the content inside a subsection
def edit_subsection(request):
    if request.method == 'POST':
        subsectonID = request.GET.get('id')
        subsection = Subsection.objects.get(id=subsectonID)
        form = SubsectionForm(request.POST, request.FILES, instance=subsection)
        if form.is_valid():
            new_subsection = form.save(commit=False)
            new_subsection.created_at = subsection.created_at
            new_subsection.lecture_section = subsection.lecture_section
            new_subsection.save()
            messages.success(request, "Subsection Update Successful")
            form = SubsectionForm()
    else:
        subsectonID = request.GET.get('id')
        subsection = Subsection.objects.get(id=subsectonID)
        form = SubsectionForm(instance=subsection)
    return render(request, 'edit-subsection.html', {'form': form})

# Uploader page
def uploader(request):
	html = "<h1>uploader</h1>"
	return HttpResponse(html)
