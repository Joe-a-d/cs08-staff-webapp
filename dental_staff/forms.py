import sys
from django import forms
from uploader.models import Announcements, Subject, LectureSection, Subsection, Content

'''
Forms.py 

Creates all the necessary forms to allow users to add and remove new content for each of the models
'''

class AnnouncementForm(forms.ModelForm):
    class Meta:
        model = Announcements
        fields = ('title', 'content')

class ModuleForm(forms.ModelForm):
    class Meta:
        model = Subject
        fields = ('title', 'year', 'image', 'moodle_link')

class LectureSectionForm(forms.ModelForm):
    class Meta:
        model = LectureSection
        fields = ('title','description')

class SubsectionForm(forms.ModelForm):
    class Meta:
        model = Subsection
        fields = ("title",)

class ContentForm(forms.ModelForm):
    class Meta:
        model = Content
        fields = ('file', 'due_date')


