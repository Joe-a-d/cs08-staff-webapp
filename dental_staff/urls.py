"""dental_staff URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls import include
import django_heroku
from dental_staff import views
from api import views as api_views
from manager import views as manager_views


api_urlpatterns = [
    path('socials/', api_views.socials_get, name="get_socials"),
    path('announcements/', api_views.announcements_get, name="get_announcements"),
    path('subjects/<int:uid>/', api_views.subjects_get, name="get_subject"),
    path('subjects/', api_views.subjects_get, name="get_subjects"),
    path('subjects/<int:uid>/textbooks/',
         api_views.textbooks_get, name="get_textbooks"),
    path('subjects/<int:uid>/lecture/',
         api_views.lectures_get, name="get_lectures"),
    path('content/', api_views.content_get, name="get_content"),
    path('content/<int:uid>/label',
         api_views.content_update, name="update_content"),
    path('content/tags', api_views.content_tag, name="create_tag"),
    path('auth/', include('rest_framework.urls')),
    path('links/', api_views.useful_link_get, name="get_links"),
    path('subjects/delete/', api_views.delete_module, name="delete_module"),
    path('content/delete/', api_views.delete_content, name="delete_content"),
    path('subsection/delete/', api_views.delete_subsection, name="delete_subsection"),
    path('section/delete/', api_views.delete_lecture_section, name="delete_lecture_section"),
]

urlpatterns = [
    path('api/', include(api_urlpatterns)),
    path('manager/', include('manager.urls')),
    path('socials_announcements/', include('socials_announcements.urls')),
    path('admin/', admin.site.urls),
    path('', views.manage_modules),
    path('manage-modules/', views.manage_modules, name="manage-modules"),
    path('add-module/', views.add_module, name="add-module"),
    path('view-module/', views.view_module, name="view-module"),
    path('add-lecture-section/', views.add_lecture_section, name="add_lecture_section"),
    path('add-subsection/', views.add_subsection, name="add-subsection"),
    path('add-content/', views.add_content, name="add-content"),
    path('edit-content/', views.edit_content, name="edit-content"),
    path('edit-subsection/', views.edit_subsection, name="edit-subsection"),
    path('manage-socials-access/', views.manage_socials_access, name="manage-socials-access"),
    path('view-posts/', views.view_posts, name="view-posts"),
    path('manage-socials/',views.manage_socials_landing, name='manage-socials'),
    path('make-an-announcement/',views.announcement, name="make-an-announcement"),
    path('login/', views.user_login, name='login'),
    path('logout/', views.user_logout, name='logout'),
    path('uploader/' , views.uploader, name='uploader'),
]
