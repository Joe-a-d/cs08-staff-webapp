from django.contrib import admin
from uploader.models import Announcements, Subject, LectureSection, Content, Subsection

'''
Admin.py

Registering the models to the db.

'''

admin.site.register(Announcements)
admin.site.register(Subject)
admin.site.register(LectureSection)
admin.site.register(Content)
admin.site.register(Subsection)
