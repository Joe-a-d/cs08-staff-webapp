// Module
function deleteModule() {
    var moduleID = document.getElementById('deleteModuleID');
    var id = moduleID.value;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            alert("Deleted Module OK!");
            var moduleTable = document.getElementById("moduleTable");
            var rowID = document.getElementById('deleteRowID');
            moduleTable.deleteRow(rowID.value);
        }
    };
    xhttp.open("GET", "https://uofgdental-production.herokuapp.com/api/subjects/delete?id=" + id, true);
    xhttp.send();

    var formID = document.getElementById('moduleForm');
    formID.style.display = 'none';
}



// Content
function deleteContent(id) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            alert("Deleted Content OK!");
        }
    };
    xhttp.open("GET", "https://uofgdental-production.herokuapp.com/api/content/delete?id=" + id, true);
    xhttp.send();

}
function editContent(contentID) {
    window.location = "../edit-content/?id=" + contentID;
}
function addLectureContent(subsectionID) {
    window.location = "../add-content/?id=" + subsectionID;
}


// Lecture Section
function deleteSection() {
    var sectionID = document.getElementById('deleteSectionID');
    var id = sectionID.value;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            alert("Deleted Section OK!");
        }
    };
    xhttp.open("GET", "https://uofgdental-production.herokuapp.com/api/section/delete?id=" + id, true);
    xhttp.send();

    var formID = document.getElementById('moduleForm');
    formID.style.display = 'none';
}
function addLectureSection() {
    var moduleID = document.getElementById("moduleID");
    var val = moduleID.innerText;
    window.location = "../add-lecture-section/?id=" + val;
}
function addLectureSubsection(sectionID) {
    window.location = "../add-subsection/?id=" + sectionID;
}


// Subsection
function deleteSubsection(id) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            alert("Deleted Subsection OK!");
        }
    };
    xhttp.open("GET", "https://uofgdental-production.herokuapp.com//api/subsection/delete?id=" + id, true);
    xhttp.send();
}
function editSubsection(subsectionID) {
    window.location = "../edit-subsection/?id=" + subsectionID;
}



//Other
function showDeletePopUp(rowNum, id) {
    var moduleID = document.getElementById('deleteModuleID');
    moduleID.value = id;
    var rowID = document.getElementById('deleteRowID');
    rowID.value =rowNum;
    var formID = document.getElementById('moduleForm');
    formID.style.display = 'block';

}

// messages div fades out after 3s
setTimeout(
    function(){
        $("#messages-container").fadeOut("slow")
    }, 3000
)
$(document).ready(function(){
    $(".userEntry").mouseenter(function(){
        $(this).css("background","lightgrey");
    });
    $(".userEntry").mouseleave(function(){
        $(this).css("background","white");
    });
});
function deleteLectureSection(id) {
    var sectionID = document.getElementById('deleteSectionID');
    sectionID.value = id;
    var formID = document.getElementById('moduleForm');
    formID.style.display = 'block';
}

function cancel() {
    var formID = document.getElementById('moduleForm');
    formID.style.display = 'none';

}

$ (function() {
    $(".tableRow").click(function () {
        window.location = $(this).data("url");
    });
});

$(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.show").each(function(){
        	$(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
        	$(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
        }).on('hide.bs.collapse', function(){
        	$(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
        });
    });


// https://docs.djangoproject.com/en/3.1/ref/csrf/

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
const csrftoken = getCookie('csrftoken');

$("button").click(function(){
	profile_id = $(this).val();
	button = $(this);
  $.ajax({
    method: "POST",
  	url: "revoke/", 
  	data : { 'profile_id' : profile_id,
    'csrfmiddlewaretoken': csrftoken },
  	dataType: 'json',
  	success: function(result){
  		button.closest('tr').remove();
  	}, 
  	error: function(result){
      alert("400")
  		// when working on the front end this alert should be removed
  		// instead div modal-hidden, should become modal-error
  		// div content will display the error
  	}

	});
});

$(".delete_social").click(function(){
  uid = $(this).id;
  button = $(this);
  $.ajax({
    method: "POST",
    url: "revoke/", 
    data : { 'uid' : uid,
    'csrfmiddlewaretoken': csrftoken },
    dataType: 'json',
    success: function(result){
      button.closest('div').remove();
    }, 
    error: function(result){
      alert("400")
      // when working on the front end this alert should be removed
      // instead div modal-hidden, should become modal-error
      // div content will display the error
    }

  });
});