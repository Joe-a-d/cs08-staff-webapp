$(document).ready(function(){

    function getCookie(name) {
        let cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            const cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i++) {
                const cookie = cookies[i].trim();
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    const csrftoken = getCookie('csrftoken');

    function renderRegisterComponents(){
        $("#id_subjects").css('height','300px').css('width','inherit').css('font-size','larger');
        $("#id_subjects").addClass("form-control");
        $("#id_email").addClass("form-control");
        // $("#id_file").addClass("custom-file-input");

        $("#id_is_lecturer").css('height','20px').css('width', '30px');
        $("#id_is_secretary").css('height','20px').css('width', '30px');
        $("#id_is_admin").css('height','20px').css('width', '30px');
    };

    renderRegisterComponents();

    function revokeUser(profileid, btn) {
        button = btn;
        $.ajax({
            method: "POST",
            url: "revoke/", 
            data : { 'profile_id' : profileid,
            'csrfmiddlewaretoken': csrftoken },
            dataType: 'json',
            success: function(result){
                button.closest('tr').remove();
                $('#revokeModal').css('display','none');
                alert("Revoked successfully");
            }, 
            error: function(result){
                $('#revokeModal').css('display','none');
                // invalid firebase_uid provided before,
                // directly remove the invalid user & profile from local DB
                // and remove user with the same email from firebase
                if (result.responseJSON.error.includes("Invalid user profile removed.")){
                    button.closest('tr').remove();
                }

                // other error response, user won't be removed from firebase & local DB
                alert(result.responseJSON.error);


                // when working on the front end this alert should be removed
                // instead div modal-hidden, should become modal-error
                // div content will display the error
            }
        });
    }


    $(".revokedBtn").click(function(){
        $('#revokeModal').css('display','block');
        $(".modal-backdrop.show").css("display","none");
        var btn = this;
        var useremail = this.parentNode.parentNode.children[2].textContent;
        $('.modal-body').text("Are you sure to revoke user " + useremail + "?");

        // update the value of confirm btn
        var profileid = this.nextElementSibling.value;
        $('#revokedConfirmBtn').val(profileid);
        $("#revokedConfirmBtn").click(
            function(){
                var profileid = $(this).val();
                revokeUser(profileid, btn);
            }
        )
        
        $('#revokedCancelBtn').click(
            function(){
                $('#revokeModal').css('display','none');
            }
        )
    });


});
