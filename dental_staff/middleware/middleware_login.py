import inspect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseForbidden


class LoginMiddleware:
    """
    Require the user to be logged in and be an admin or secretary to access
    views, redirecting her to the login page if she's not.

    For excluded views see :meth:`.process_view`


    See `DOCS <https://docs.djangoproject.com/en/dev/topics/http/middleware>`_
    """

    def __init__(self, get_response):
        """
        Called once, after runserver
        """
        self.get_response = get_response

    def __call__(self, request):
        """
        Called each time a view is called
        """
        return self.get_response(request)

    def is_lecturer(self, user, view):
        """
        Checks that the user is a lecturer
        
        Arguments:
            user {User} -- the request user
            view {str} -- the module of the view that triggered the middleware as a string
        
        Returns:
            bool 
        """
        if (not (user.is_staff or user.is_superuser) and
        view not in ['uploader.views', 'dental_staff.views']):
            return True 
        return False
    
    def is_secretary(self, user, view):
        """
        Checks that the user is a secretary.
        A secretary can only access "managing access page" i.e. register or revoke users
        
        Arguments:
            user {User} -- the request user
            view {str} -- the module of the view that triggered the middleware as a string
        
        Returns:
            bool 
        """
        # dental_staff.views - login/logout/...
        # manager.views - register/revoke users on/from firebase
        if (not (user.is_superuser) and view not in ['dental_staff.views', 'manager.views']):
            return True
        return False

    def process_view(self, request, view_func, view_args, view_kwargs):
        """
        middleware hook called before the view is called but after the view
        going to be called as been determined. Hence, we use it here since
        we need to implement the middleware functionality selectively,
        excluding the views part of the API or the login view.

        See `DOCS <https://docs.djangoproject.com/en/dev/topics/http/middleware/#process-view>`_
        """

        app_views = inspect.getmodule(view_func).__name__

        if app_views == 'api.views':
            return None

        if view_func.__name__ == 'user_login':
            return None

        if request.user.is_authenticated:
            if self.is_lecturer(request.user, app_views):
                return HttpResponseForbidden("Not Allowed")
            if self.is_secretary(request.user, app_views):
                return HttpResponseForbidden("Not Allowed")
            return None

        return login_required(view_func)(request, *view_args, **view_kwargs)
