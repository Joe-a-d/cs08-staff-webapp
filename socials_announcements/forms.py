from django.forms import ModelForm
from uploader.models import Socials, UsefulLink
from django.core.exceptions import ValidationError
import re

'''
Forms.py 

Allows users to link twitter and instagram accounts
'''

class SocialsForm(ModelForm):
    class Meta:
        model = Socials
        fields = ['twitter', 'instagram']

    def clean(self):
        super(SocialsForm, self).clean()
        invalid = []
        for k,v in self.cleaned_data.items():
            if v == None:
                continue
            if k == 'twitter':
                valid = re.match('\w+',v)
            else:
                valid = re.match('[\w+.]',v) 
            if not valid:
                invalid.append(k)
        if invalid:
            raise ValidationError([
                ValidationError(f"Invalid {*invalid,} username")
                ])
        return self.cleaned_data


class UsefulLinksForm(ModelForm):
    class Meta:
        model = UsefulLink
        fields = ['name', 'url']
