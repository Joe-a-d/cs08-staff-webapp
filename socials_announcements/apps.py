from django.apps import AppConfig

'''
Apps.py 

Registering socials_announcement app
'''

class SocialsAnnouncementsConfig(AppConfig):
    name = 'socials_announcements'
