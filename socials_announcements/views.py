from django.http import HttpResponse
from django.shortcuts import render
from django.contrib import messages
from uploader.models import Socials
from .forms import SocialsForm, UsefulLinksForm
import requests
import os
import json
from django.http import HttpResponseServerError
from django.core.exceptions import ObjectDoesNotExist

def socials(request):
    """Handles Socials object creation
    
    This view renders a form based on the Socials model. 
    It receives data from the form and validates it. Errors are returned via
    django messages

    The main logic consists of :
    1. checking that the forms fields are not None, since we allow the user to
        register a single social network profile
    2. call a helper to handle API calls for each social network, which returns
        the data we are interested in
    3. creates a new django object with the usernames passed in the form
    4. render a new template, passing the data from both helpers as context

    When both calls are unsuccessful, no django object is created
    
    Arguments:
        request {HttpRequest} 
    
    Returns:
        [HttpResponse] -- an HttpResponse object whose context contains
        at least one of:
            - "tweet_data" : a dic containing the payload from `get_twitter`
            - "instagram_data" : a dic containg the payload from `get_instagram`

            if the post request was successful

        [HttpResponse] -- an HttpResponse object; context = empty form if GET

        [HttpResponse] -- an HttpResponse object; context = form with errors 
        or form with django messages if POST failed
            
        

    """
    ERROR_MESSAGE = ("Something went wrong when trying to fetch those profiles"
    ", are you sure that the user profile exists and is public?")
    if request.method == "POST":
        socials_form = SocialsForm(request.POST)
        if socials_form.is_valid():
            context = {}
            if socials_form.cleaned_data['twitter'] != None:
                twitter_handle = socials_form.cleaned_data['twitter']
                response = get_twitter(twitter_handle)
                if response:
                    context['tweet_data']=response
            if socials_form.cleaned_data['instagram'] != None:
                instagram_handle = socials_form.cleaned_data['instagram']
                instagram_url = f"https://www.instagram.com/{instagram_handle}"
                context['instagram_data']=instagram_url
            if context:
                socials_form.save()
                return render(request, 'manage-socials-access.html', context=context)
            messages.error(request,ERROR_MESSAGE)
            return render(request, 'manage-socials-access.html', context={
                'form': SocialsForm(),})
        else:
            messages.error(request, socials_form.errors)
            return render(request, 'manage-socials-access.html', context={
                'form': SocialsForm(),})
    else:
        return render(request, 'manage-socials-access.html', context={
            'form': SocialsForm(),})


# HELPERS

def get_twitter(twitter_handle):
    """Gets the user data and tweet data via two API calls and returns the 
    data required as a dictionary
    
    1. The first API call gets the user by username and the filter includes 
    the profile image

    2. The second API call gets the user last 10 tweets and the filter 
    include tweet metrics (e.g. retweet count)
    
    Arguments:
        twitter_handle {str} -- twitter username passed in form
    
    Returns:
        response [dict] -- {
                profile_image : {str} , profile image url
                twitter_metrics : {
                        "retweet_count": {int},
                        "reply_count": {int},
                        "like_count": {int},
                        "quote_count": {int}
                        }
                twitter_handle : {str} , username

                }

        None -- either the call does to return 200 or the user tweets are 
            protected
    """
    token_twitter = os.getenv('TOKEN_TWITTER')
    endpoint_twitter_data = "https://api.twitter.com/2/users/"
    endpoint_twitter_user = "https://api.twitter.com/2/users/by/username/"
    headers_twitter = {"Authorization": f"Bearer {token_twitter}"}
    user_response = requests.get(endpoint_twitter_user+twitter_handle,
        params={"user.fields":'profile_image_url,protected'},
        headers=headers_twitter)
    if user_response.status_code != 200:
        return None
    try:
        user_response.json()['errors']
        return None
    except:
        pass
    user_payload = user_response.json()['data']
    uid = user_payload['id']
    if user_payload['protected'] == True:
        return None
    tweet_response = requests.get(f"{endpoint_twitter_data}{uid}/tweets", 
        params={'tweet.fields':'public_metrics'},
        headers=headers_twitter)
    tweet_payload = tweet_response.json()
    try:
        twitter_metrics = tweet_payload['data'][0]['public_metrics']
        twitter_text = tweet_payload['data'][0]['text']
    except:
        twitter_metrics = 'this user has no tweets' 
        twitter_text = ''
    response = {'profile_image':user_payload['profile_image_url'],
        'twitter_metrics': twitter_metrics,'twitter_handle': twitter_handle,
        'twitter_text': twitter_text}
    return response


def links(request):
    SUCCESS_MESSAGE = "The URL was successfully added to the database."
    if request.method == "POST":
        links_form = UsefulLinksForm(request.POST)
        if links_form.is_valid():
            links_form.save()
            messages.success(request, SUCCESS_MESSAGE)
        return render(
            request, 'links.html',
            context={'form': links_form})
    links_form = UsefulLinksForm()
    return render(
            request, 'links.html',
            context={'form': links_form})

        
def revoke(request):
    """
    Handles the ajax post request to delete social objects from DB

    Gets the id of the object from the request, tries to fetch it and delete 
    it from DB logging an unexpected exception to the server logs if it fails

    Arguments:
        request {HTTP} -- an HTTP request with `uid` as a query param

    Returns:
        {JsonResponse} 

        -- 200 if either the user was deleted or there was 
        nothing to delete

        -- 400 otherwise
    """
    uid = request.POST.get('uid')  # noqa: E101

    ERROR_MESSAGE = 'Something went wrong while trying to process your request'
    try:
        obj = Socials.objects.get(id=uid)
        obj.delete()
    except ObjectDoesNotExist as e:
        pass
    except Exception as e:
        # TODO log exception
        return HttpResponseServerError(ERROR_MESSAGE)
    return HttpResponse(200)
