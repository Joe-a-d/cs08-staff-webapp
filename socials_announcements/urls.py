from django.urls import path
from . import views

'''
Urls.py 

Registers social_announcements urls
'''

app_name = 'socials_announcements'

urlpatterns = [
    path('socials/', views.socials, name="socials"),
    path('links/', views.links, name="links"),
    path('revoke/', views.revoke, name="revoke"),
]
